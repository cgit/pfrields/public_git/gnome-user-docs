<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="power-closelid">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware#power"/>
    <link type="seealso" xref="power-suspendfail"/>
    
    <desc>Laptops go to sleep when you close the lid, in order to save power.</desc>
    <revision pkgversion="0.1" version="0.1" date="2011-03-19" status="incomplete"/>
    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    
    <include href="legal.xml" xmlns="http://www.w3.org/2001/XInclude"/>
  </info>

<title>Why does my computer turn off when I close the lid?</title>

<p>When you close the lid of your laptop, your computer will <em>suspend</em> in order to save power. This means that the computer is not actually turned off - it has just gone to sleep. You can wake it up by opening the lid. If it doesn't wake up, try clicking the mouse or pressing a key. If that still doesn't work, press the power button.</p>

<p>Some computers are unable to suspend properly, normally because their hardware isn't completely supported by the operating system (e.g. the Linux drivers are incomplete). In this case, you may find that you are unable to wake-up your computer after you've closed the lid. You can try to <link xref="power-suspendfail">fix the problem with suspend</link>, or you can prevent the computer from trying to suspend when you close the lid.</p>

<section id="nosuspend">
 <title>Stop the computer from suspending when the lid is closed</title>
 <p>If you don't want the computer to suspend when you close the lid...</p>
 <!-- FIXME: Need to complete this section! -->
</section>

  <comment>
   <cite date="2010-06-29" href="mailto:gnome-doc-list@gnome.org">GNOME Documentation Project</cite>
   <p>Explain that the computer probably hasn't turned off, it just suspended itself. Describe how to wake up from being suspended. Explain that some computers don't suspend properly. Show how you can change the settings for what happens when the lid is closed.</p>
  </comment>
	
</page>
