<page xmlns="http://projectmallard.org/1.0/"
      type="topic" id="files-preview-music">

  <info>
    <link type="guide" xref="files#faq"/>
    
    <desc>Move the mouse pointer over a music file and it will start playing.</desc>

    <revision pkgversion="3.0" version="1.0" date="2011-03-20" status="review"/>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>

  </info>

<title>Quickly preview music/sound files</title>

<p>Move your mouse pointer over a music file (or other sound file). A musical note icon will appear and the song will start playing. Move the mouse away from the file and the song will stop playing.</p>

<p>Only music files which are in a supported format can be played in this way. </p>

<p>Music files on network shares cannot be previewed in this way by default. To change this, click <guiseq><gui>Edit</gui><gui>Preferences</gui></guiseq> and go to the <gui>Preview</gui> tab. Change the <gui>Preview sound files</gui> option to <gui>Always</gui>.</p>

</page>
