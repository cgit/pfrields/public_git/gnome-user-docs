<page xmlns="http://projectmallard.org/1.0/"
      type="topic" 
      id="mouse-middleclick">

<info>
 <desc>Copy/paste, close tabs, open urls.. </desc>

 <revision pkgversion="3.0" version="1.0" date="2011-03-19" status="review"/>
 
 <link type="guide" xref="tips" />

 <credit type="author">
  <name>Tiffany Antopolski</name>
  <email>tiffany.antopolski@gmail.com</email>
 </credit>
 <license><p>Creative Commons Share Alike 3.0</p></license>
</info>

<title>Middle click</title>

<p>Your desktop has a built in copy and paste function:</p>
<steps>
<item><p>Highlight the text you want to copy.</p></item>
<item><p>Go to where you want to paste it and press the middle mouse button.</p></item>
<item><p>The text is pasted at the mouse position.</p>
<p>The text must be highlighted when you middle click in order to paste.  </p></item>
</steps>  

<list>
<item><p>You can middle click on a hyperlink in a browser to have it open in a new tab.</p></item>
<item><p>In <app>Firefox</app> middle clicking on a tab will immediately close it.</p></item>
</list>
<section id="two-button">
 <title>Two button mouse</title>
 <p>To perform a "middle click" on a two button mouse, click both mouse buttons at the same time.</p>
<p>On a laptop, clicking both pads simultaneously will simulate middle click behavior.</p>
</section>

<section id="scroll-wheel">
 <title>Scroll Wheel</title>
 <p>Clicking the scroll wheel will mimic the middle click.</p>
</section>
</page>
