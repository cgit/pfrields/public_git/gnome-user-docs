<page xmlns="http://projectmallard.org/1.0/"
      type="topic" id="files-move">

  <info>
    <link type="guide" xref="files-arrange"/>
    <link type="seealso" xref="files-copy"/>
    <desc>Relocate a file or folder to a new location.</desc>

    <revision pkgversion="3.0" version="1.0" date="2010-06-16" status="candidate"/>
    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>

  </info>

<title>Moving files and folders</title>

<p>
A file or folder can be moved to a new location by dragging and dropping with the mouse, or you can use the cut and paste commands.  This could, for example, allow you to sort files saved in your <gui> Downloads </gui> folder into other folders for long term storage.
</p>

<steps>
<title>Drag files to the new location</title>
<item><p><link xref="open-file-manager">Open a file browser</link> to the folder containing the item you want to move.</p></item>
<item><p>Open a second file manager window by clicking <guiseq><gui>Places</gui><gui>Home Folder</gui></guiseq> in the main menubar.</p></item>
<item><p>In the second window, navigate to where you want to move the item.</p></item>
<item><p>Click on the item and drag it to its new destination.</p></item>
</steps>

<steps>
<title>Cut and paste to the new location</title>
<item><p>Select the item you want to move by clicking on it once.</p></item>
<item><p>Right-click <guiseq><gui>Edit</gui><gui>Cut</gui></guiseq>.</p></item>
<item><p>Navigate to where you want to move the item.</p></item>
<item><p>Right-click <guiseq><gui>Edit</gui><gui>Paste</gui></guiseq>.</p></item>
</steps>
</page>
