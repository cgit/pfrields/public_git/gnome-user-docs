<page xmlns="http://projectmallard.org/1.0/"
      type="topic" 
      style="tip" 
      id="look-background">

  <info>
    <link type="guide" xref="prefs#display"/>
    
    <desc>How to set an image as your desktop background.</desc>
    <revision pkgversion="3.0" version="0.1" date="2011-03-13" status="review"/>
    
    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email>loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
  </info>

<title>Change the desktop background</title>

<p>You can change the background directly from the desktop. To do this, right-click anywhere on the desktop, making sure not to click on an icon, and choose the <gui>Change Desktop Background</gui> option. </p>

<p>A more indirect approach would be clicking on your name in the upper-right corner, and choose <gui>System Settings</gui> and select <gui>Preferences</gui>. Then, in the <gui>Personal</gui> section, choose <gui>Background</gui>. </p>

<p>Under the <gui>Background tab</gui>, select an image for the desktop background. The selected image will be superimposed on the desktop background color. </p>

<p>You can use a color instead of an image as a desktop background. To set the desktop background color, select a color from the available options. A solid color or a gradient of two colors may be used. </p>

<p>To finish, just click <gui>Close</gui>. </p>

<note style="tip">
 <p>When using an image as a background, the desktop background color becomes visible if the image is transparent or if the image does not cover the entire desktop.</p>
</note>

</page>
