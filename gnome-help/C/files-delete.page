<page xmlns="http://projectmallard.org/1.0/"
      type="topic" id="files-delete">

  <info>
    <link type="guide" xref="files#delete"/>
    <desc>Remove files or folders you no longer need.</desc>

    <revision pkgversion="3.0" version="1.0" date="2010-06-16" status="candidate"/>
    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>

  </info>

<title>Deleting files and folders</title>

<p>
If you don't want a file or folder any more, you can delete it.
</p>

<p>
When you delete an item it is moved to the Trash folder, where it is stored until you empty the trash.  Items stored in the Trash folder can be restored to their original location if you decide you need them, or if they were accidentally deleted.
</p>

<steps>
  <title>Delete a file or folder move it to the trash folder)</title>
  <item><p>Select the item you want to delete by clicking it once.</p></item>
  <item><p>Press the <gui>Delete</gui> key on your keyboard.</p></item>
</steps>

<p>
You can permanently delete a file, without having to send it to the Trash folder first.
</p>

<steps>
  <title>Permanently delete a file or folder</title>
  <item><p>Select the item you want to delete.</p></item>
  <item><p>Press and hold the <gui>Shift</gui> key, then press the <gui>Delete</gui> key on your keyboard.</p></item>
</steps>
</page>
