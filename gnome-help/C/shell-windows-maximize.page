<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      style="task"
      id="shell-windows-maximize">

  <info>

    <link type="guide" xref="shell-windows#working-with-windows"/>

    <desc>See how you can maximize, restore, resize, arrange and hide windows.</desc>
    <revision pkgversion="3.0" version="0.1" date="2011-03-19" status="outdated"/>
    <!-- See shell-windows-states -->
    
    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include href="legal.xml" xmlns="http://www.w3.org/2001/XInclude"/>
  </info>

  <title>Maximize and unmaximize (restore) a window</title>

    <p> To maximize or unmaximize a window, you can:</p>
     <list>
       <item>
       <p>
        To maximize a window, click on the <gui>title bar</gui> of an application, and drag it to the top of the screen.  When the <gui>mouse pointer</gui> touches the very top of the screen, the entire screen becomes hilighted.  Release the mouse button to maximize the screen.
       </p>
      <item>

      </item>
       <p>
        To restore the window to its original size, click on the <gui>title bar</gui> of the application, and drag it down from the <gui>top bar</gui>.  After the window separates from the <gui>top bar</gui> it will restore itself to an unmaximized state.
       </p>
      </item>
    </list>

    <note style="tip">
      <p>
        Pressing <keyseq><key>Alt</key><key>right-click</key></keyseq> anywhere in a window will allow you to move the window. Some people may find this easier than needing to click in the <gui>title bar</gui> of an application.
      </p>
      <p>
         You can also use your keyboard to maximize a window. Press <keyseq><key>Alt</key><key>Space</key></keyseq> to bring up the window menu, and then press <key>x</key>.
      </p>
    </note>

</page>
