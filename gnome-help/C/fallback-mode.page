<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="fallback-mode">

  <info>
    <link type="guide" xref="index#apps"/>
    <desc>If your computer's graphics card doesn't support certain features, a more basic version of the desktop will be started.</desc>
    <revision pkgversion="3.0" version="0.1" date="2011-03-19" status="review"/>
    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include href="legal.xml" xmlns="http://www.w3.org/2001/XInclude"/>
  </info>

<title>What is fallback mode?</title>

<p>If your computer's graphics card doesn't support certain features, a more basic version of the desktop will be displayed and you will see a message telling you what happened. This is called <em>fallback mode</em>, and it allows you to use GNOME on your computer without some of its more advanced features.</p>

<p>Most of the features that are missing in fallback mode are related to organizing windows and starting applications. For example, instead of having an activities overview, you will have an <gui>Applications menu</gui> at the top of the screen which you can use to start apps, and a list of open windows at the bottom of the screen. Running in fallback mode doesn't affect which applications you can run - it only changes the way that the desktop looks.</p>

<p>Starting in fallback mode doesn't necessarily mean that your graphics card isn't good enough to run GNOME - it might just mean that you don't have the right drivers installed for your graphics card. If you can find better graphics card drivers for your computer, you may be able to run the full version of GNOME.</p>

</page>
