<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      style="task"
      id="shell-windows-switching">

  <info>
  
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>
    
    <desc>Learn how to quickly switch between windows, using things like the Alt-Tab switcher.</desc>
    <revision pkgversion="3.0" version="0.1" date="2011-03-19" status="review"/>
    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    
    <include href="legal.xml" xmlns="http://www.w3.org/2001/XInclude"/>
  </info>

<title>Switch between windows</title>

<terms>

 <item>
    <title>From a workspace:</title>
    
    
<list>
<item><p>
Press <keyseq><key>Alt</key><key>Tab</key></keyseq> to switch between windows.
</p>
</item>

<item><p>
Press <keyseq><key>Alt</key><key>Shift</key><key>Tab</key></keyseq> to cycle through the application windows in reverse.
</p>

<note style="tip">
<p>
 The windows are grouped by application and the previews of the applications with multiple windows are available as you click through. The previews show up after a short delay, but you can get them immediately by clicking the ↓ arrow key. 
</p>
</note>


<note>
<p>
The applications running on other workspaces are displayed after a vertical separator. Including all applications in the <keyseq><key>Alt</key><key>Tab</key></keyseq> makes switching between tasks a single step process and provides you with a full picture of what applications are running. 
</p>
</note>
</item>

<item><p> 
You can move between the preview windows with the → or ← arrow keys or with the mouse pointer.
</p></item>
<item><p> 
You can also use the keys <key>w</key>, <key>a</key>, <key>s</key>, <key>d</key> (for ↑←↓→ respectively) instead of the arrow keys for one-handed operation. 
</p></item>
<item><p> 
 Previews of applications with a single window are only available when the ↓ arrow is explicitly hit. 
</p></item>
<item><p> 
 
     It is possible to switch to any window by moving to it with the mouse and clicking. 
</p></item>
<item><p>
  <key>Alt</key> + they key above the <key>Tab</key> switches between windows of the same application in <keyseq><key>Alt</key><key>Tab</key></keyseq>.  The <key>`</key> is above the <key>Tab</key>.
</p></item>
</list>

</item>



<item>
  <title>From the <gui>Activities</gui> overview:</title>
<p>
  Click on a <link xref="shell-windows">window</link> to switch to it and leave the overview. If you have multiple <link xref="shell-windows-workspaces">workspaces</link> open, you can click on each workspace to view the open windows on each workspace.
  </p>
  </item>
</terms>

</page>
